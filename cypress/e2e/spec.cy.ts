describe('empty spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000')

    cy.contains('Добавить список').click()
    cy.get('input[placeholder="Название списка"]').type('Спорт').should('have.value', 'Спорт')
    cy.contains('Добавить').click()

    cy.get('li:last-child span').should('have.text', 'Спорт')

    cy.contains('Добавить список').click()
    cy.get('input[placeholder="Название списка"]').type('Книги').should('have.value', 'Книги')
    cy.get('i:last-child i').click()
    cy.contains('Добавить').click()

    cy.get('li:last-child span').should('have.text', 'Книги')
    cy.get('li:last-child span').click()

    cy.contains('Новая задача').click()
    cy.get('input[placeholder="Текст задачи"]')
      .type('Прочитать "Война и мир"')
      .should('have.value', 'Прочитать "Война и мир"')
    cy.contains('Добавить задачу').click()

    cy.get('div:last-child p').should('have.text', 'Прочитать "Война и мир"')
  })
})
