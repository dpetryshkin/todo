const path = require('path')
module.exports = {
  webpack: {
    configure: {
      experiments: {
        topLevelAwait: true
      }
    },
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  }
}
