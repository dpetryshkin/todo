start:
	yarn start

install:
	yarn install

build:
	yarn build

test:
	yarn test:cy

lint:
	yarn lint

format:
	yarn format

server:
	yarn server