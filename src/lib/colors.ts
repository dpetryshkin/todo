import { BadgeColorType } from '@/interfaces/badge'

const colors: Color[] = [
  {
    name: 'grey'
  },
  {
    name: 'lime'
  },
  {
    name: 'purple'
  },
  {
    name: 'black'
  },
  {
    name: 'red'
  },
  {
    name: 'green'
  },
  {
    name: 'blue'
  },
  {
    name: 'pink'
  }
]

interface Color {
  name: BadgeColorType
}

export default colors
