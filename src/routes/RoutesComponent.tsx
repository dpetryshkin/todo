import { Route, Routes, useLocation } from 'react-router-dom'
import { useEffect } from 'react'
import { useNavigate } from 'react-router'
import Home from '@/pages/Home'

const RoutesComponent = () => {
  const location = useLocation()
  const navigate = useNavigate()

  const loader = () => {
    if (location.pathname === '/') {
      navigate('/tasks/all')
    }
  }

  useEffect(() => {
    loader()
  }, [location])

  return (
    <Routes>
      <Route path="/tasks/:taskID" element={<Home />} />
    </Routes>
  )
}

export default RoutesComponent
