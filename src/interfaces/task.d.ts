import { BadgeColorType } from '@/interfaces/badge'

export interface Task {
  id: number
  title: string
  color: BadgeColorType
}

export interface SideBarTask {
  id: number | string
  title: string
  color?: BadgeColorType
}
