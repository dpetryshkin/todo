export type BadgeColorType =
  | 'grey'
  | 'lime'
  | 'purple'
  | 'black'
  | 'red'
  | 'green'
  | 'blue'
  | 'pink'
