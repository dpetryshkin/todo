export interface IGetTaskArgs {
  id: string | number
}

export interface ITaskData {
  id?: number
  title: string
  color: string
}

export interface IGetTodoArgs {
  id: string | number
}

export interface ITodoData {
  id?: number
  label: string
  taskId: number
  completed: boolean
}
