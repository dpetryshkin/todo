export interface Todo {
  id: number
  label: string
  taskId: number
  completed: boolean
}
