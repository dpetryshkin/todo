import { BrowserRouter } from 'react-router-dom'
import RoutesComponent from '@/routes/RoutesComponent'

const App = () => {
  console.log('test')

  return (
    <div id="app">
      <BrowserRouter>
        <RoutesComponent />
      </BrowserRouter>
    </div>
  )
}

export default App
