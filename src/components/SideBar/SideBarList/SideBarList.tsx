import { FC } from 'react'
import { useParams } from 'react-router'
import { ListItem } from '@/components/SideBar/ListItem/ListItem'
import { SideBarTask } from '@/interfaces/task'
import styles from './SideBarList.module.scss'

interface SideBarListProps {
  tasks: SideBarTask[]
  onRemove: (id: number) => void
}

export const SideBarList: FC<SideBarListProps> = ({ tasks, onRemove }) => {
  const params = useParams()

  const isActive = (id?: number | string): boolean => {
    const taskID = params.taskID
    if (!taskID) {
      return false
    } else if (id === 'all' && isNaN(+taskID)) {
      return true
    } else if (!isNaN(+taskID)) {
      return +taskID === id
    }
    return false
  }

  const getHref = (id?: number | string): string => {
    return id ? '/tasks/' + id : '/'
  }

  const handleRemove = (id: number | string) => {
    if (typeof id === 'string') return
    onRemove(id)
  }

  const isRemovable = (id: number | string): boolean => {
    return id !== 'all'
  }

  return (
    <ul className={styles.list}>
      {tasks.map((t) => (
        <ListItem
          key={t.id}
          label={t.title}
          url={getHref(t.id)}
          active={isActive(t.id)}
          color={t.color}
          removable={isRemovable(t.id)}
          onRemove={() => handleRemove(t.id)}
        />
      ))}
    </ul>
  )
}
