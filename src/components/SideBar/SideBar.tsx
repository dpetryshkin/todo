import { FC } from 'react'
import { SideBarList } from '@/components/SideBar/SideBarList/SideBarList'
import { AddList } from '@/components/SideBar/AddList/AddList'
import { SideBarTask } from '@/interfaces/task'
import { ITaskData } from '@/interfaces/rest'
import styles from './SideBar.module.scss'

interface AppSideBarProps {
  tasks: SideBarTask[]
  addTask: (data: ITaskData) => void
  removeTask: (id: number) => void
}

export const SideBar: FC<AppSideBarProps> = ({ tasks, addTask, removeTask }) => {
  return (
    <div className={styles.sidebar}>
      <SideBarList tasks={tasks} onRemove={removeTask} />
      <AddList onSubmit={addTask} />
    </div>
  )
}
