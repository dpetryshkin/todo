import { FC, MouseEvent } from 'react'
import cn from 'classnames'
import { Badge } from '@/components/Badge/Badge'
import { BadgeColorType } from '@/interfaces/badge'
import list from '@/assets/images/list.svg'
import remove from '@/assets/images/remove.svg'
import styles from './ListItem.module.scss'
import { useNavigate } from 'react-router'

interface ListItemProps {
  label: string
  url: string
  color?: BadgeColorType
  active?: boolean
  removable?: boolean
  onRemove?: () => void
}

export const ListItem: FC<ListItemProps> = ({
  label,
  url,
  active = false,
  removable = true,
  color,
  onRemove
}) => {
  const navigate = useNavigate()
  const clickHandler = (e: MouseEvent<HTMLElement>) => {
    e.stopPropagation()
    e.preventDefault()
    if (onRemove) {
      onRemove()
    }
  }

  const onNavigate = () => {
    navigate(url)
  }

  return (
    <li className={cn(styles.listItem, { [styles.active]: active })} onClick={onNavigate}>
      {color ? (
        <Badge color={color} />
      ) : (
        <i>
          <img src={list} alt="list-img" />
        </i>
      )}
      <span>{label}</span>
      {removable && (
        <img className={styles.removeIcon} src={remove} alt="remove-img" onClick={clickHandler} />
      )}
    </li>
  )
}
