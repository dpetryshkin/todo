import { FC } from 'react'
import cn from 'classnames'
import add from '@/assets/images/add.svg'
import styles from './AddListButton.module.scss'

interface AddListButtonProps {
  onClick: () => void
}

export const AddListButton: FC<AddListButtonProps> = ({ onClick }) => {
  return (
    <div className={cn(styles.addButton)} onClick={onClick}>
      <i>
        <img src={add} alt="add-img" />
      </i>
      <span>Добавить список</span>
    </div>
  )
}
