import { Badge } from '@/components/Badge/Badge'
import { Button } from '@/components/Button/Button'
import { FC, useState } from 'react'
import { TextInput } from '@/components/TextInput/TextInput'
import colors from '@/lib/colors'
import close from '@/assets/images/close.svg'
import styles from './AddListForm.module.scss'

interface AddListFormProps {
  onClose: () => void
  onTaskAdd: (value: string, color: string) => void
}

export const AddListForm: FC<AddListFormProps> = ({ onTaskAdd, onClose }) => {
  const [value, setValue] = useState<string>('')
  const [currenColor, setCurrentColor] = useState<string>(colors[0].name)

  const clickHandler = () => {
    onTaskAdd(value, currenColor)
  }

  return (
    <div className={styles.popup}>
      <img src={close} className={styles.closeBtn} alt="Close button" onClick={onClose} />
      <TextInput placeholder="Название списка" value={value} onChange={setValue} />
      <div className={styles.colors}>
        {colors.map((c) => (
          <Badge
            key={c.name}
            color={c.name}
            active={c.name === currenColor}
            className={styles.badge}
            onSelect={setCurrentColor}
          />
        ))}
      </div>
      <Button label="Добавить" className={styles.button} onClick={clickHandler} />
    </div>
  )
}
