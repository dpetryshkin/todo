import { FC, useState } from 'react'
import { AddListForm } from '@/components/SideBar/AddListForm/AddListForm'
import { AddListButton } from '@/components/SideBar/AddListButton/AddListButton'
import { ITaskData } from '@/interfaces/rest'

interface AddListProps {
  onSubmit: (data: ITaskData) => void
}

export const AddList: FC<AddListProps> = ({ onSubmit }) => {
  const [visible, setVisible] = useState<boolean>(false)

  const showAddListForm = () => {
    setVisible(true)
  }

  const hideAddListForm = () => {
    setVisible(false)
  }

  const handleSubmit = (value: string, color: string) => {
    onSubmit({ title: value, color })
    setVisible(false)
  }

  return (
    <div>
      {visible ? (
        <AddListForm onClose={hideAddListForm} onTaskAdd={handleSubmit} />
      ) : (
        <AddListButton onClick={showAddListForm} />
      )}
    </div>
  )
}
