import { ChangeEvent, FC } from 'react'
import cn from 'classnames'
import styles from './TextInput.module.scss'

interface TextInputProps {
  value: string
  placeholder?: string
  onChange: (e: string) => void
  className?: string
}

export const TextInput: FC<TextInputProps> = ({ value, placeholder, onChange, className }) => {
  const changeHandler = (e: ChangeEvent<HTMLInputElement>): void => {
    onChange(e.currentTarget.value)
  }

  return (
    <input
      value={value}
      className={cn(styles.field, className)}
      type="text"
      autoFocus
      placeholder={placeholder}
      onChange={changeHandler}
    />
  )
}
