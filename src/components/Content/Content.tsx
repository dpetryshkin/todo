import { FC } from 'react'
import { ContentTask } from '@/components/ContentTask/ContentTask'
import { AddTask } from '@/components/ContentTask/AddTask/AddTask'
import { ITaskData, ITodoData } from '@/interfaces/rest'
import { Task } from '@/interfaces/task'
import { Todo } from '@/interfaces/todo'
import styles from './Content.module.scss'

interface AppContentProps {
  tasks: Task[]
  todos: Todo[]
  visibleAdd: boolean
  onChange: (data: ITodoData, id: number) => void
  addTodo: (data: ITodoData) => void
  removeTodo: (id: number) => void
  updateTask: (data: ITaskData, id: number) => void
}

export const Content: FC<AppContentProps> = ({
  tasks,
  todos,
  visibleAdd,
  onChange,
  addTodo,
  removeTodo,
  updateTask
}) => {
  const filteredTodo = (taskId: number) => {
    return todos.filter((t) => t.taskId === taskId)
  }

  const submitHandler = (value: string) => {
    const data = {
      label: value,
      taskId: tasks[0].id,
      completed: false
    }
    addTodo(data)
  }

  const saveTitle = (title: string, id: number) => {
    const task = tasks.find((t) => t.id === id)
    if (!task) return
    const data = {
      ...task,
      title
    }
    updateTask(data, id)
  }

  return (
    <div className={styles.todo}>
      <div className={styles.tasks}>
        {tasks.map((task) => (
          <ContentTask
            key={task.id}
            title={task.title}
            color={task.color}
            todos={filteredTodo(task.id)}
            onChange={onChange}
            removeTodo={removeTodo}
            saveTitle={(title) => saveTitle(title, task.id)}
          />
        ))}
      </div>
      {visibleAdd && <AddTask onSubmit={submitHandler} />}
    </div>
  )
}
