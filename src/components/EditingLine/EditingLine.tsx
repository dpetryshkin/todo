import { FC } from 'react'
import { TextInput } from '@/components/TextInput/TextInput'
import { IconButton } from '@/components/IconButton/IconButton'
import check from '@/assets/images/check.svg'
import remove from '@/assets/images/remove.svg'
import styles from './EditingLine.module.scss'

interface EditingLineProps {
  value: string
  setValue: (e: string) => void
  onCancel: () => void
  onSave: () => void
}

export const EditingLine: FC<EditingLineProps> = ({ value, setValue, onCancel, onSave }) => {
  return (
    <div className={styles.line}>
      <TextInput value={value} onChange={setValue} className={styles.input} />
      <div className={styles.buttons}>
        <IconButton icon={check} onClick={onSave} />
        <IconButton icon={remove} onClick={onCancel} />
      </div>
    </div>
  )
}
