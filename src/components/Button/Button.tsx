import { FC } from 'react'
import cn from 'classnames'
import styles from './Button.module.scss'

interface AppButtonProps {
  label: string
  secondary?: true
  onClick?: () => void
  className?: string
}

export const Button: FC<AppButtonProps> = ({ label, secondary, onClick, className }) => {
  return (
    <button
      className={cn(styles.button, { [styles.grey]: secondary }, className)}
      onClick={onClick}
    >
      {label}
    </button>
  )
}
