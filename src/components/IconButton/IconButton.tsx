import { FC } from 'react'
import styles from './IconButton.module.scss'

interface IconButtonProps {
  icon: string
  onClick: () => void
}

export const IconButton: FC<IconButtonProps> = ({ icon, onClick }) => {
  return (
    <div className={styles.button} onClick={onClick}>
      <img src={icon} alt="" />
    </div>
  )
}
