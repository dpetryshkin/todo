import { FC } from 'react'
import cn from 'classnames'
import { BadgeColorType } from '@/interfaces/badge'
import styles from './Badge.module.scss'

interface AppBadgeProps {
  color: BadgeColorType
  active?: boolean
  onSelect?: (e: BadgeColorType) => void
  className?: string
}

export const Badge: FC<AppBadgeProps> = ({ color, active = false, onSelect, className }) => {
  const clickHandler = (): void => {
    if (onSelect) {
      onSelect(color)
    }
  }

  return (
    <i>
      <i
        className={cn(styles.badge, styles[color], { [styles.active]: active }, className)}
        onClick={clickHandler}
      ></i>
    </i>
  )
}
