import { FC, useState } from 'react'
import { EditingLine } from '@/components/EditingLine/EditingLine'
import { IconButton } from '@/components/IconButton/IconButton'
import edit from '@/assets/images/edit.svg'
import { BadgeColorType } from '@/interfaces/badge'
import styles from './TaskTitle.module.scss'

interface TaskTitleProps {
  title: string
  color: BadgeColorType
  onSave: (value: string) => void
}

export const TaskTitle: FC<TaskTitleProps> = ({ title, color, onSave }) => {
  const [isEdit, setIsEdit] = useState(false)
  const [value, setValue] = useState(title)

  const handleCancel = () => {
    setIsEdit(false)
    setValue(title)
  }

  const handleSave = () => {
    setIsEdit(false)
    onSave(value)
  }

  return (
    <div className={styles.wrapper}>
      {isEdit ? (
        <EditingLine
          value={value}
          setValue={setValue}
          onCancel={handleCancel}
          onSave={handleSave}
        />
      ) : (
        <div className={styles.title} style={{ color }}>
          {title}
          <div>
            <IconButton icon={edit} onClick={() => setIsEdit(true)} />
          </div>
        </div>
      )}
    </div>
  )
}
