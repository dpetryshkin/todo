import { FC } from 'react'
import add from '@/assets/images/add.svg'
import styles from './AddTaskButton.module.scss'

interface AddTaskButtonProps {
  onOpen: (e: true) => void
}

export const AddTaskButton: FC<AddTaskButtonProps> = ({ onOpen }) => {
  const clickHandler = () => {
    onOpen(true)
  }

  return (
    <div className={styles.formNew} onClick={clickHandler}>
      <img src={add} alt="Add icon" />
      <span>Новая задача</span>
    </div>
  )
}
