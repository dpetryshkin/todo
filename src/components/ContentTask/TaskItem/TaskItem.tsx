import { FC, useState } from 'react'
import { TaskCheckbox } from '@/components/ContentTask/TaskCheckbox/TaskCheckbox'
import { TaskLabel } from '@/components/ContentTask/TaskLabel/TaskLabel'
import { TaskActions } from '@/components/ContentTask/TaskActions/TaskActions'
import { EditingLine } from '@/components/EditingLine/EditingLine'
import styles from './TaskItem.module.scss'

interface TaskItemProps {
  label: string
  completed: boolean
  onChange: (label: string, completed: boolean) => void
  onRemove: () => void
  onSave: (label: string) => void
}

export const TaskItem: FC<TaskItemProps> = ({ label, completed, onChange, onRemove, onSave }) => {
  const [isEdit, setIsEdit] = useState<boolean>(false)
  const [value, setValue] = useState<string>(label)

  const changeHandler = (e: boolean) => {
    onChange(label, e)
  }

  const onEdit = () => {
    setIsEdit(true)
  }

  const handleCancel = () => {
    setIsEdit(false)
    setValue(label)
  }

  const handleSave = () => {
    setIsEdit(false)
    onSave(value)
  }

  return (
    <div className={styles.items}>
      {isEdit ? (
        <EditingLine
          value={value}
          setValue={setValue}
          onCancel={handleCancel}
          onSave={handleSave}
        />
      ) : (
        <div className={styles.row}>
          <TaskCheckbox completed={completed} onChange={changeHandler} />
          <TaskLabel label={label} />
          <TaskActions onEdit={onEdit} onRemove={onRemove} className={styles.actions} />
        </div>
      )}
    </div>
  )
}
