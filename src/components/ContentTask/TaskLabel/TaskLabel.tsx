import { FC } from 'react'
import styles from './TaskLabel.module.scss'

interface TaskLabelProps {
  label: string
}

export const TaskLabel: FC<TaskLabelProps> = ({ label }) => {
  return <p className={styles.label}>{label}</p>
}
