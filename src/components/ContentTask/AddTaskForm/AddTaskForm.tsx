import { FC, useState } from 'react'
import { Button } from '@/components/Button/Button'
import { TextInput } from '@/components/TextInput/TextInput'
import styles from './AddTaskForm.module.scss'

interface AddTaskFormProps {
  onSubmit: (e: string) => void
  onClose: (e: false) => void
}

export const AddTaskForm: FC<AddTaskFormProps> = ({ onSubmit, onClose }) => {
  const [value, setValue] = useState<string>('')

  const onCancel = () => {
    onClose(false)
  }

  const submitHandler = () => {
    onSubmit(value)
  }

  return (
    <div className={styles.form}>
      <TextInput value={value} placeholder="Текст задачи" onChange={setValue} />
      <div className={styles.buttons}>
        <Button label="Добавить задачу" onClick={submitHandler} />
        <Button label="Отмена" secondary onClick={onCancel} />
      </div>
    </div>
  )
}
