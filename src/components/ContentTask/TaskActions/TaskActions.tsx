import { FC } from 'react'
import cn from 'classnames'
import edit from '@/assets/images/edit.svg'
import remove from '@/assets/images/remove.svg'
import { IconButton } from '@/components/IconButton/IconButton'
import styles from './TaskActions.module.scss'

interface TaskActionsProps {
  onEdit: () => void
  onRemove: () => void
  className?: string
}

export const TaskActions: FC<TaskActionsProps> = ({ onRemove, onEdit, className }) => {
  return (
    <div className={cn(styles.actions, className)}>
      <IconButton icon={edit} onClick={onEdit} />
      <IconButton icon={remove} onClick={onRemove} />
    </div>
  )
}
