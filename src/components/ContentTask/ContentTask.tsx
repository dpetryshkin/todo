import { TaskTitle } from '@/components/ContentTask/TaskTitle/TaskTitle'
import { TaskItem } from '@/components/ContentTask/TaskItem/TaskItem'
import { Todo } from '@/interfaces/todo'
import { FC } from 'react'
import { BadgeColorType } from '@/interfaces/badge'
import { ITodoData } from '@/interfaces/rest'

interface AppTaskProps {
  title: string
  color: BadgeColorType
  todos: Todo[]
  onChange: (data: ITodoData, id: number) => void
  removeTodo: (id: number) => void
  saveTitle: (value: string) => void
}

export const ContentTask: FC<AppTaskProps> = ({
  title,
  color,
  todos,
  onChange,
  removeTodo,
  saveTitle
}) => {
  const handleChange = (l: string, c: boolean, id: number) => {
    const todo = todos.find((t) => t.id === id)
    if (!todo) return
    const data = {
      id,
      label: l,
      completed: c,
      taskId: todo.taskId
    }
    onChange(data, id)
  }

  const updateTodoLabel = (label: string, id: number) => {
    const todo = todos.find((t) => t.id === id)
    if (!todo) return
    const data = {
      ...todo,
      label
    }
    onChange(data, id)
  }

  return (
    <>
      <TaskTitle title={title} color={color} onSave={saveTitle} />
      {todos.map((t) => (
        <TaskItem
          key={t.id}
          label={t.label}
          completed={t.completed}
          onChange={(l, c) => handleChange(l, c, t.id)}
          onRemove={() => removeTodo(t.id)}
          onSave={(label) => updateTodoLabel(label, t.id)}
        />
      ))}
    </>
  )
}
