import { FC, useState } from 'react'
import { AddTaskButton } from '@/components/ContentTask/AddTaskButton/AddTaskButton'
import { AddTaskForm } from '@/components/ContentTask/AddTaskForm/AddTaskForm'

interface AddTaskProps {
  onSubmit: (value: string) => void
}

export const AddTask: FC<AddTaskProps> = ({ onSubmit }) => {
  const [visible, setVisible] = useState<boolean>(false)

  const handleSubmit = (value: string): void => {
    onSubmit(value)
    setVisible(false)
  }

  return (
    <>
      {visible ? (
        <AddTaskForm onClose={setVisible} onSubmit={handleSubmit} />
      ) : (
        <AddTaskButton onOpen={setVisible} />
      )}
    </>
  )
}
