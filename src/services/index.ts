import ServerHttp from '@/services/http'
import Api from '@/services/api'

export const baseUrl = process.env.REACT_APP_BASE_URL

const http = new ServerHttp()
export const api = new Api(http.get())
