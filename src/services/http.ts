import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
import { baseUrl } from '@/services/index'

class API {
  public config: AxiosRequestConfig

  public instance: AxiosInstance

  constructor() {
    this.config = {
      baseURL: baseUrl
    }

    this.instance = axios.create({
      ...this.config
    })
  }

  get() {
    return this.instance
  }
}

export default API
