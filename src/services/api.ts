import { AxiosInstance } from 'axios'

import { IGetTaskArgs, IGetTodoArgs, ITaskData, ITodoData } from '@/interfaces/rest'
import { Task } from '@/interfaces/task'
import { Todo } from '@/interfaces/todo'

class Api {
  private _apiInstance: AxiosInstance

  constructor(apiInstance: AxiosInstance) {
    this._apiInstance = apiInstance
  }

  deleteTodo(id: number | string) {
    const url = 'todos/' + id
    return this.delete(url)
  }

  updateTodo(data: ITodoData, id: number) {
    const url = 'todos/' + id
    return this.patch(url, data)
  }

  addTodo(data: ITodoData): Promise<Todo> {
    const url = 'todos'
    return this.post(url, data)
  }

  getTodos(args?: IGetTodoArgs): Promise<Todo[]> {
    const url = 'todos'
    return this.get(url, args)
  }

  deleteTask(id: number | string) {
    const url = 'tasks/' + id
    return this.delete(url)
  }

  updateTask(data: ITaskData, id: number): Promise<Task> {
    const url = 'tasks/' + id
    return this.patch(url, data)
  }

  addTask(data: ITaskData): Promise<Task> {
    const url = 'tasks'
    return this.post(url, data)
  }

  getTasks(args?: IGetTaskArgs): Promise<Task[]> {
    const url = 'tasks'
    return this.get(url, args)
  }

  async delete(url: string) {
    try {
      const response = await this._apiInstance.delete(url)
      if (response?.status === 200) {
        return response.data
      }
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  async patch(url: string, data: object) {
    try {
      const response = await this._apiInstance.patch(url, data)
      if (response?.status === 200) {
        return response.data
      }
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  async post(url: string, data: object) {
    try {
      const response = await this._apiInstance.post(url, data)
      if (response?.status === 201) {
        return response.data
      }
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  async get(url: string, args?: object) {
    try {
      const response = await this._apiInstance.get(url, { params: args })
      if (response?.status === 200) {
        return response.data
      }
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}

export default Api
