import { useEffect, useMemo, useState } from 'react'
import { useNavigate, useParams } from 'react-router'
import { api } from '@/services'
import { Content } from '@/components/Content/Content'
import { SideBar } from '@/components/SideBar/SideBar'
import { SideBarTask, Task } from '@/interfaces/task'
import { Todo } from '@/interfaces/todo'
import { ITaskData, ITodoData } from '@/interfaces/rest'

const allTask = {
  id: 'all',
  title: 'Все задачи'
}

const Home = () => {
  const navigate = useNavigate()
  const params = useParams()
  const [tasks, setTasks] = useState<Task[]>([])
  const [todos, setTodos] = useState<Todo[]>([])

  const loadTasks = async () => {
    const data = await api.getTasks()
    setTasks(data)
  }

  const loadTodos = async () => {
    const data = await api.getTodos()
    setTodos(data)
  }

  useEffect(() => {
    loadTasks()
    loadTodos()
  }, [])

  const visible = useMemo(() => {
    return params.taskID !== 'all'
  }, [params])

  const filteredTasks = useMemo(() => {
    const taskID = params.taskID
    if (taskID && !isNaN(+taskID)) {
      return tasks.filter((t) => t.id === +taskID)
    } else {
      return tasks
    }
  }, [tasks, params])

  const filteredTodos = useMemo(() => {
    return todos.filter((t) => filteredTasks.find((task) => task.id === t.taskId))
  }, [todos, filteredTasks])

  const taskList = useMemo<SideBarTask[]>(() => {
    return [allTask, ...tasks]
  }, [tasks])

  const setNavigate = (id: number) => {
    if (id.toString() === params.taskID) {
      navigate('/tasks/all')
    }
  }

  const handleSubmit = async (data: ITaskData) => {
    try {
      const task = await api.addTask(data)
      const newTasks = [...tasks, task]
      setTasks(newTasks)
    } catch (e) {
      console.log(e)
    }
  }

  const handleRemove = async (id: number) => {
    try {
      await api.deleteTask(id)
      const newTasks = tasks.filter((t) => t.id !== id)
      const newTodos = todos.filter((t) => t.taskId !== id)
      setTasks(newTasks)
      setTodos(newTodos)
      setNavigate(id)
    } catch (e) {
      console.log(e)
    }
  }

  const handleUpdate = async (data: ITodoData, id: number) => {
    try {
      const res = await api.updateTodo(data, id)
      const newTodos = todos.map((t) => {
        if (t.id === id) {
          return { ...res }
        } else {
          return {
            ...t
          }
        }
      })
      setTodos(newTodos)
    } catch (e) {
      console.log(e)
    }
  }

  const createTodo = async (data: ITodoData) => {
    try {
      const res = await api.addTodo(data)
      const newTodos = [...todos, res]
      setTodos(newTodos)
    } catch (e) {
      console.log(e)
    }
  }

  const removeTodo = async (id: number) => {
    try {
      await api.deleteTodo(id)
      const newTodos = todos.filter((t) => t.id !== id)
      setTodos(newTodos)
    } catch (e) {
      console.log(e)
    }
  }

  const updateTask = async (data: ITaskData, id: number) => {
    try {
      const res = await api.updateTask(data, id)
      const newTasks = tasks.map((t) => {
        if (t.id === id) {
          return { ...res }
        } else {
          return { ...t }
        }
      })
      setTasks(newTasks)
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <div className="home">
      <SideBar tasks={taskList} addTask={handleSubmit} removeTask={handleRemove} />
      <Content
        tasks={filteredTasks}
        todos={filteredTodos}
        visibleAdd={visible}
        onChange={handleUpdate}
        addTodo={createTodo}
        removeTodo={removeTodo}
        updateTask={updateTask}
      />
    </div>
  )
}

export default Home
